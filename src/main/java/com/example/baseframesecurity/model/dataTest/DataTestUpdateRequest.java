package com.example.baseframesecurity.model.dataTest;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DataTestUpdateRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    @Length(min = 13, max = 13)
    private String phone;

    @NotNull
    private Double price;

    @NotNull
    private Integer age;
}
