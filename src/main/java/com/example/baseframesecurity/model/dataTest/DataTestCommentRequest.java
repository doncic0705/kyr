package com.example.baseframesecurity.model.dataTest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DataTestCommentRequest {
    @ApiModelProperty(notes = "댓글", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String comment;
}
