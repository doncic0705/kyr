package com.example.baseframesecurity.model.dataTest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataTestListItem {
    private Long id;
    private String name;
    private String phone;
}
