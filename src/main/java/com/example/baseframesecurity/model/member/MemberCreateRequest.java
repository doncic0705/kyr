package com.example.baseframesecurity.model.member;

import com.example.baseframesecurity.enums.MemberGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberCreateRequest {
    @ApiModelProperty(notes = "회원 그룹", required = true)
    @NotNull
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "아이디", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @ApiModelProperty(notes = "비밀번호 확인", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String contact;

    @ApiModelProperty(notes = "적립금", required = true)
    @NotNull
    private Double point;
}
