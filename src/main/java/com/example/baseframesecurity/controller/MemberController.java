package com.example.baseframesecurity.controller;

import com.example.baseframesecurity.entity.Member;
import com.example.baseframesecurity.enums.MemberGroup;
import com.example.baseframesecurity.model.common.CommonResult;
import com.example.baseframesecurity.model.member.MemberCreateRequest;
import com.example.baseframesecurity.service.MemberDataService;
import com.example.baseframesecurity.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;

    @ApiOperation(value = "회원 등록")
    @PostMapping("data")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest memberCreateRequest) {
        memberDataService.setMember(memberCreateRequest.getMemberGroup(), memberCreateRequest);
        return ResponseService.getSuccessResult();
    }
}
