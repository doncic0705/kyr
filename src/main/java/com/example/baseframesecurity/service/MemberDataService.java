package com.example.baseframesecurity.service;

import com.example.baseframesecurity.entity.Member;
import com.example.baseframesecurity.enums.MemberGroup;
import com.example.baseframesecurity.exception.CWrongPhoneNumberException;
import com.example.baseframesecurity.lib.CommonCheck;
import com.example.baseframesecurity.model.member.MemberCreateRequest;
import com.example.baseframesecurity.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "12345678";
        String contact = "010-0000-0000";
        Double point = 0D;
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setContact(contact);
            createRequest.setPoint(point);
            createRequest.setName("최고관리자");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    public Member setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername()))
            throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe()))
            throw new CWrongPhoneNumberException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        return memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;

    }
}
