package com.example.baseframesecurity.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
